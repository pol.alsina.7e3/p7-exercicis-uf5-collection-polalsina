﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */
using System;
using System.IO;

namespace P7_UF5_AlsinaPol
{
    class Program
    {
        static void Main()
        {
            Menu program = new Menu();
            program.MenuController();
        }
    }
}
