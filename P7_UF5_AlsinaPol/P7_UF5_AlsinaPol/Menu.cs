﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */
using P7_UF5_AlsinaPol.Ex1;
using P7_UF5_AlsinaPol.Ex2;
using P7_UF5_AlsinaPol.Ex3;
using System;
using System.Collections.Generic;
using System.Text;

namespace P7_UF5_AlsinaPol
{
    class Menu
    {

        public void MenuController()
        {
            bool finish;
            do
            {
                Console.WriteLine("1 - Ex1 Sistema de atención al cliente en un banco");
                Console.WriteLine("2 - Ex2 Sistema de reparto de Just Feed");
                Console.WriteLine("3 - Ex3 Super torneo de cartas Yu-Gi-Oh!");
                finish = MenuOptions();
            } while (finish != true);
        }

        public bool MenuOptions()
        {
            int answer = Convert.ToInt32(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    ProvesEx1 ex1 = new ProvesEx1();
                    ex1.Proves();
                    break;
                case 2:
                    ProvesEx2 ex2 = new ProvesEx2();
                    ex2.Proves();
                    break;
                case 3:
                    Joc joc = new Joc();
                    joc.Jugar();
                    break;
                case 0:
                    return true;
            }
            return false;
        }


    }
}
