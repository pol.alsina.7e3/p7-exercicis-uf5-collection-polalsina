﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex2
{
    public class CuaDeRepartiment : Queue<Comanda>
    {
        public CuaDeRepartiment() {  }
        public void AfegirComanda(string nomClient, string direccioEntrega, List<string> plats)
        {
            Comanda novaComanda = new Comanda(nomClient, direccioEntrega, plats);
            Enqueue(novaComanda);
        }

        public void MostrarSeguentComanda()
        {
            Console.WriteLine("Seguent comanda:");
            Console.WriteLine(Peek());
        }

        public void EliminarSeguentComanda()
        {
            Console.WriteLine("Comanda entregada:");
            Console.WriteLine(Dequeue());
        }
    }
}
