﻿using System;
using System.Collections.Generic;
/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex2
{
    public class Comanda
    {
        public string nomClient { get; set; }
        public string direccioEntrega { get; set; }
        public List<string> plats { get; set; }

        public Comanda(string nomClient, string direccioEntrega, List<string> plats)
        {
            this.nomClient = nomClient;
            this.direccioEntrega = direccioEntrega;
            this.plats = plats;
        }

        public override string ToString()
        {
            return $"Client: {nomClient}, Direccio de entrega: {direccioEntrega}, Plats: {string.Join(", ", plats)}";
        }
    }
}
