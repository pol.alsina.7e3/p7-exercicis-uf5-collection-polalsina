﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex2
{
    public class ProvesEx2
    {
        public void Proves()
        {
            InData data = new InData();
            CuaDeRepartiment cuaRepartiment = new CuaDeRepartiment();
            bool exit = false; 
            while (!exit)
            {
                string nomClient = data.LlegirString("Ingresa el nom del client:");


                string direccioEntrega = data.LlegirString("Ingresa la direccio de l'entrega:");

                string platsString = data.LlegirString("Ingresa els plats a demanar (separats per una coma):");
                List<string> plats = new List<string>(platsString.Split(','));

                cuaRepartiment.AfegirComanda(nomClient, direccioEntrega, plats);
                string opcio = data.LlegirString("¿Desitja veure la proxima comanda en la cua? (s/n)");

                if (opcio.ToLower() == "s")
                {
                    cuaRepartiment.MostrarSeguentComanda();
                }

                opcio = data.LlegirString("Has entregat la seguent comanda? (s/n)");

                if (opcio.ToLower() == "s")
                {
                    cuaRepartiment.EliminarSeguentComanda();
                }

                Console.WriteLine($"Solicituds pendents en la cua: {cuaRepartiment.Count}");
                string resposta = data.LlegirString("Vols continuar? (s/n) ");
                if (resposta.ToLower() == "n")
                {
                    exit = true;
                }
            }
        }
    }
}
