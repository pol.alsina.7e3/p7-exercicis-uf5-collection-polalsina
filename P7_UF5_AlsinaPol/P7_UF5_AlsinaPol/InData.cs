﻿/*
 * Author: Pol Alsina
 * Date: 0805/2023
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P7_UF5_AlsinaPol
{
    public class InData
    {
        public string LlegirString(string msg)
        {
            string resposta;
            do
            {
                Console.WriteLine(msg);
                resposta = Console.ReadLine();
            } while (String.IsNullOrEmpty(resposta));
            return resposta;
        }
    }
}
