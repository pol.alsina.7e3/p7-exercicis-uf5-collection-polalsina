﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex1
{
    public class ProvesEx1
    {
        
        public void Proves()
        {
            InData data = new InData();
            CuaClient cua = new CuaClient();
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("Ingresi una nova solicitud de atencio al client:");
                string nomClient = data.LlegirString("Nom del client: ");

                string tipusSolicitud = data.LlegirString("Tipus de solicitud: ");
                cua.AfeigirSolicitud(nomClient, tipusSolicitud);
                cua.MostrarSeguentSolicitud();

                string resposta = data.LlegirString("Has ates aquesta solicitud? (S/N): ");
                if (resposta.ToLower() == "s")
                {
                    cua.EliminarSolicitudAtesa();
                    Console.WriteLine("Solicitud eliminada de la cua.");
                }
                cua.SolicitudPendent();
                resposta = data.LlegirString("Vols ingresar una altre solicitud? (S/N): ");
                if (resposta.ToLower() == "n")
                {
                    exit = true;
                }
            }
        }
        
    }
}
