﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex1
{
    public class CuaClient : Queue<Solicitud>
    {
        public CuaClient() { }

        public void AfeigirSolicitud(string nomClient, string tipusSolicitud)
        {
            Solicitud solicitud = new Solicitud(nomClient, tipusSolicitud);
            this.Enqueue(solicitud);
        }

        public void MostrarSeguentSolicitud()
        {
            Solicitud solicitud = this.Peek();
            Console.WriteLine("La siguiente solicitud de atención es: {0} - {1}", solicitud.NomClient, solicitud.TipusSolicitud);
        }

        public void EliminarSolicitudAtesa()
        {
            this.Dequeue();
        }

        public void SolicitudPendent()
        {
            Console.WriteLine("Hay {0} solicitudes de atención pendientes en la cola.", this.Count);
        }
    }
}
