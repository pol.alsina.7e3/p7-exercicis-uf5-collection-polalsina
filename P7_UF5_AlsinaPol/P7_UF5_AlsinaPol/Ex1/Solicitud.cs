﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex1
{
    public class Solicitud
    {
        public string NomClient { get; set; }
        public string TipusSolicitud { get; set; }

        public Solicitud(string nomClient, string tipusSolicitud)
        {
            NomClient = nomClient;
            TipusSolicitud = tipusSolicitud;
        }
    }
}
