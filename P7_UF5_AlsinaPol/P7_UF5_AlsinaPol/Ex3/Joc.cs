﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */
namespace P7_UF5_AlsinaPol.Ex3
{
    public class Joc
    {
        public void Jugar()
        {
            BarallaCartas barrallaDeCartas = new BarallaCartas();

            barrallaDeCartas.AfegirCarta(new Carta("Viego", "Espectre Huma", 4, "Quan Mata un altre carta es converteix en ella durant uns torns"));
            barrallaDeCartas.AfegirCarta(new Carta("MegaFulgor", "Hechizo", 10, "Gran poderosa magia"));
            barrallaDeCartas.AfegirCarta(new Carta("Alonso", "NanoGod", 33, "Mata a tothom"));
            Carta sainz = new Carta("Sainz", "Jr", 0, "Mata a la carta Alonso");

            MaJugador maJugador = new MaJugador();
            if (!barrallaDeCartas.EstaBuit())
            {
                maJugador.cartas.Add(barrallaDeCartas.RobarCarta());
                maJugador.cartas.Add(barrallaDeCartas.RobarCarta());
                maJugador.cartas.Add(barrallaDeCartas.RobarCarta());
                maJugador.cartas.Add(barrallaDeCartas.RobarCarta());
            }
   
            if (!maJugador.EstaBuit())
            {
                maJugador.JugarCarta(maJugador.cartas[0]);
            }
            maJugador.DescartarCarta(maJugador.cartas[0]);
            maJugador.DescartarCarta(maJugador.cartas[0]);
            maJugador.DescartarCarta(sainz);


            
        }
        
    }
}
