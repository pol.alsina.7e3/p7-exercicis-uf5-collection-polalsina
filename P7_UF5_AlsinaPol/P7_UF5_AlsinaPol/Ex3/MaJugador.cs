﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */


namespace P7_UF5_AlsinaPol.Ex3
{
    public class MaJugador
    {
        public List<Carta> cartas;

        public MaJugador()
        {
            this.cartas = new List<Carta>();
        }

        public void JugarCarta(Carta carta)
        {
            if (this.cartas.Contains(carta))
            {
                Console.WriteLine("Jugant carta: " + carta.nom);
                this.cartas.Remove(carta);
            }
            else
            {
                Console.WriteLine("La carta " + carta.nom + " no está en la teva baralla.");
            }
        }
        public void DescartarCarta(Carta carta)
        {
            if (this.cartas.Contains(carta))
            {
                Console.WriteLine("Descartant carta: " + carta.nom);
                this.cartas.Remove(carta);
            }
            else
            {
                Console.WriteLine("La carta " + carta.nom + " no esta en la teva baralla.");
            }
        }

        public bool EstaBuit()
        {
            return (this.cartas.Count == 0);
        }
    }
}
