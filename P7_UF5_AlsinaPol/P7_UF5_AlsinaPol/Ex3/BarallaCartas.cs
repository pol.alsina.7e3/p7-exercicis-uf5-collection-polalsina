﻿using System;
using System.Collections.Generic;
/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex3
{
    public class BarallaCartas : Stack<Carta>
    {
        public BarallaCartas() : base() { }

        public void AfegirCarta(Carta carta)
        {
            this.Push(carta);
        }

        public Carta RobarCarta()
        {
            if (this.Count == 0)
            {
                Console.WriteLine("No hi han mes cartas en la baralla.");
                return null;
            }
            return this.Pop();
        }

        public bool EstaBuit()
        {
            return (this.Count == 0);
        }
    }
}
