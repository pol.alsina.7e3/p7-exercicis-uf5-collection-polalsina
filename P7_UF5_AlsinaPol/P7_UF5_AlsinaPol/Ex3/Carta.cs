﻿/*
 * Author: Pol Alsina
 * Date: 08/05/2023
 */

namespace P7_UF5_AlsinaPol.Ex3
{
    public class Carta
    {
        public string nom;
        public string tipus;
        public int nivell;
        public string efecte;

        public Carta(string nom, string tipus, int nivell, string efecte)
        {
            this.nom = nom;
            this.tipus = tipus;
            this.nivell = nivell;
            this.efecte = efecte;
        }
    }
}
